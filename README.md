# About

This project aim to get familiar with [containerlab](https://containerlab.dev/).


# How it works

A lab totpology from [lab.clab.yml](https://gitlab.com/n2258/containerlab/-/blob/main/lab/lab.clab.yml) is deployed on the remote machine via [gitlab CI](https://gitlab.com/n2258/containerlab/-/blob/main/.gitlab-ci.yml).
